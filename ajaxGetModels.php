<?php
namespace TireSync;
require_once 'app/autoloaders.php';

$apiClient = new ApiClientCommon($_GET['year'], $_GET['make']);
$modelQuery = $apiClient->buildQueryString(ApiClientCommon::API_QUERY_KEY_MODELS);
$apiResponse = json_decode($apiClient->getApiResponse($modelQuery));
echo json_encode($apiResponse->items);
?>