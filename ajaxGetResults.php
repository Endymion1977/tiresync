<?php
namespace TireSync;
require_once 'app/autoloaders.php';

$apiClient = new ApiClientCommon($_GET['year'], $_GET['make'], $_GET['model'], $_GET['options']);
$fitmentsQuery = $apiClient->buildQueryString(ApiClientCommon::API_QUERY_KEY_VEHICLE_FITMENTS);
$apiResponse = json_decode($apiClient->getApiResponse($fitmentsQuery));
$apiResponse->items['notes'] = $apiResponse->notes;
echo json_encode($apiResponse->items);
?>