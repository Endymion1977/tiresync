<?php
namespace TireSync;
require_once 'app/autoloaders.php';

$apiClient = new ApiClientCommon($_GET['year'], $_GET['make'], $_GET['model']);
$optionsQuery = $apiClient->buildQueryString(ApiClientCommon::API_QUERY_KEY_OPTIONS);
$apiResponse = json_decode($apiClient->getApiResponse($optionsQuery));
echo json_encode($apiResponse->items);
?>