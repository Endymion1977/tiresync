<?php

spl_autoload_register(function($className) {
    
    // Explode canonical class name to namespace and actual class name
    list($domain, $restClassName) = explode('\\', $className, 2);
    
    $fullPath = "src/classes/{$restClassName}.php";
    
    // If the class file is found, include it
    if (is_file($fullPath)) {
        require_once $fullPath;
    }
});
