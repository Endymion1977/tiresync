<?php
namespace TireSync;

class ApiClientCommon {

    const API_BASE_URL = 'http://api.tiresync.com';
    const API_VERSION = 'v1';
    const API_DB = 'oe';
    const API_KEY = '1111-1111-1111-1111';
    const API_REQUESTER_HOST = 'localhost';

    const API_QUERY_KEY_YEARS = 'years';
    const API_QUERY_KEY_MAKES = 'makes';
    const API_QUERY_KEY_MODELS = 'models';
    const API_QUERY_KEY_OPTIONS = 'options';
    const API_QUERY_KEY_VEHICLE_FITMENTS = 'vehicle_fitments';

    private $year;
    private $make;
    private $model;
    private $options;

    public function __construct($year = null, $make = null, $model = null, $options = null) {
        $this->year = $year;
        $this->make = $make;
        $this->model = $model;
        $this->options = $options;
    }

    public function buildQueryString ($command) {
        // common part of the URL:
        $qs = '/' . self::API_VERSION . '/' . self::API_DB . '/' . $command . '/' . self::API_KEY . '/' . base64_encode(self::API_REQUESTER_HOST);

        // command-specific part:
        switch ($command) {
            case self::API_QUERY_KEY_YEARS: 
                break;
            case self::API_QUERY_KEY_MAKES:
                $qs .= '/' . base64_encode($this->year);
                break;
            case self::API_QUERY_KEY_MODELS:
                $qs .= '/' . base64_encode($this->year) . '/' . base64_encode($this->make);
                break;
            case self::API_QUERY_KEY_OPTIONS:
                $qs .= '/' . base64_encode($this->year) . '/' . base64_encode($this->make) . '/' . base64_encode($this->model);
                break;
            case self::API_QUERY_KEY_VEHICLE_FITMENTS:
                $qs .= '/' . base64_encode($this->year) . '/' . base64_encode($this->make) . '/' . base64_encode($this->model) . '/' . base64_encode($this->options);
                break;
        }
        return $qs;
    }

    public function getApiResponse ($queryString) {
        $response = file_get_contents(self::API_BASE_URL . $queryString);
        return $response;
    }

}
