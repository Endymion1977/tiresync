<?php
namespace TireSync;
require_once 'app/autoloaders.php';

$apiClient = new ApiClientCommon;
$yearsQuery = $apiClient->buildQueryString(ApiClientCommon::API_QUERY_KEY_YEARS);
$apiResponse = json_decode($apiClient->getApiResponse($yearsQuery));
echo json_encode($apiResponse->items);
?>