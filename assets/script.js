$(document).ready(function(){
    
    $('div#results').hide();
    $('div#loading_results').hide();

    $.ajax({   
      type: 'GET',        
      dataType: "json",                           
      url: 'ajaxGetYears.php',              
      beforeSend: function() {
          $('#select_year').empty().append($("<option></option>").attr("value", 'null').text('Loading years from API...'));
      },
      success: yearsReceived,
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
 
  $('#select_year').on('change', function() {
  	$('div#results').hide();
    var $el = $("#select_make"); $el.empty();
    $el = $("#select_model"); $el.empty();
    $el = $("#select_option"); $el.empty();
    $.ajax({   
      type: 'GET',        
      dataType: "json",                           
      url: 'ajaxGetMakes.php?year='+$('#select_year').val(),              
      beforeSend: function() {
          $('#select_make').empty().append($("<option></option>").attr("value", 'null').text('Loading makes from API...'));
      },
      success: makesReceived,
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
  });

  $('#select_make').on('change', function() {
  	$('div#results').hide();
    var $el = $("#select_model"); $el.empty();
    $el = $("#select_option"); $el.empty();
    $.ajax({   
      type: 'GET',        
      dataType: "json",                           
      url: 'ajaxGetModels.php?year='+$('#select_year').val()+'&make='+$('#select_make').val(),              
      beforeSend: function() {
          $('#select_model').empty().append($("<option></option>").attr("value", 'null').text('Loading models from API...'));
      },
      success: modelsReceived,
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
  });

  $('#select_model').on('change', function() {
  	$('div#results').hide();
    var $el = $("#select_option"); $el.empty();    
    $.ajax({   
      type: 'GET',        
      dataType: "json",                           
      url: 'ajaxGetOptions.php?year='+$('#select_year').val()+'&make='+$('#select_make').val()+'&model='+$('#select_model').val(),              
      beforeSend: function() {
          $('#select_option').empty().append($("<option></option>").attr("value", 'null').text('Loading options from API...'));
      },
      success: optionsReceived,
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });
  });

  $('#select_option').on('change', function() {
    $.ajax({   
      type: 'GET',        
      dataType: "json",                           
      url: 'ajaxGetResults.php?year='+$('#select_year').val()+'&make='+$('#select_make').val()+'&model='+$('#select_model').val()+'&options='+$('#select_option').val(),              
      beforeSend: function() {
      	  $('div#results').hide();
          $('div#loading_results').show();
          $('span#full_model').text($('#select_year').val() + ' ' + $('#select_make').val() + ' ' + $('#select_model').val() + ' ' + $('#select_option').val());
      },
      success: resultsReceived,
      error: function(jqXHR, textStatus, errorThrown) {
        console.log(JSON.stringify(jqXHR));
        console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
      }
    });    
  });

});

function yearsReceived(result) {
  var $el = $("#select_year");
  $el.empty();
  $el.append($("<option></option>").attr("value", null).text('-- Select year: --'));
  $.each(result, function(key, value) {
    $el.append($("<option></option>").attr("value", value).text(value));
  });
};

function makesReceived(result) {
  var $el = $("#select_make");
  $el.empty();
  $el.append($("<option></option>").attr("value", null).text('-- Select make: --'));
  $.each(result, function(key, value) {
    $el.append($("<option></option>").attr("value", value).text(value));
  });
};

function modelsReceived(result) {
  var $el = $("#select_model");
  $el.empty();
  $el.append($("<option></option>").attr("value", null).text('-- Select model: --'));
  $.each(result, function(key, value) {
    $el.append($("<option></option>").attr("value", value).text(value));
  });
};

function optionsReceived(result) {
  var $el = $("#select_option");
  $el.empty();
  $el.append($("<option></option>").attr("value", null).text('-- Select option: --'));
  $.each(result, function(key, value) {
    $el.append($("<option></option>").attr("value", value).text(value));
  });
};

function resultsReceived(result) {
  $('div#loading_results').hide();
  $('span#section_width').text(result[0]["SECTIONWIDTH"]);
  $('span#aspect_ratio').text(result[0]["ASPECTRATIO"]);
  $('span#rim').text(result[0]["RIM"]);
  $('span#notes').text(result["notes"]);
  $('div#results').show();
};