<?php
namespace TireSync;
require_once 'app/autoloaders.php';

$apiClient = new ApiClientCommon($_GET['year']);
$makeQuery = $apiClient->buildQueryString(ApiClientCommon::API_QUERY_KEY_MAKES);
$apiResponse = json_decode($apiClient->getApiResponse($makeQuery));
echo json_encode($apiResponse->items);
?>