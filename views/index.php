<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset=utf-8>
    <link type="text/css" rel="stylesheet" href="assets/stylesheet.css"/>
    <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type='text/javascript' src='assets/script.js'></script>
    <title>TireSync Demo</title>
  </head>
  <body>
    <div>Year: <select id="select_year"> <option value="null">--</option></select></div>
    <div>Make: <select id="select_make"> <option value="null">--</option></select></div>
    <div>Model: <select id="select_model"> <option value="null">--</option></select></div>
    <div>Option: <select id="select_option"> <option value="null">--</option></select></div>

    <div id="loading_results">
      <br><br>Loading results from API...<br><br>
    </div>

    <div id="results">
      <br><br>Standard tire size information for a <span id="full_model"></span> is<br><br>
      Section width: <span id="section_width"></span><br>
      Aspect ratio: <span id="aspect_ratio"></span><br>
      Rim: <span id="rim"></span><br><br>
      Notes: <span id="notes"></span>
    </div>
  </body>
</html>